import React from "react";
import {
  BrowserRouter as ReactRouter,
  Route as ReactRoute,
  Switch
} from "react-router-dom";

import routes from "./routes";

const Router = () => {
  const routesArray = routes.map(route => {
    return (
      <ReactRoute
        component={route.component}
        to={route.link}
        key={"route_" + route.id + "_" + route.name}
      ></ReactRoute>
    );
  });

  return (
    <ReactRouter>
      <Switch>{routesArray}</Switch>
    </ReactRouter>
  );
};

export default Router;
