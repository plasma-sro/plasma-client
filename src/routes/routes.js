import React from "react";
import Home from "../components/views/home/home";

const routes = [
  {
    id: 0,
    link: "/",
    name: "home",
    component: () => {
      return <Home></Home>;
    }
  }
];

export default routes;
