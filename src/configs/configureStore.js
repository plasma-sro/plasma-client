import { applyMiddleware, compose, createStore } from "redux";
import rootReducer from "../reducers/rootReducer";
import middle from "../services/middle";

const configureStore = () => {
  const initialState = {};
  const middleware = [middle];
  const enhancers = [];

  return createStore(
    rootReducer(),
    initialState,
    compose(applyMiddleware(...middleware), ...enhancers)
  );
};

export default configureStore;
