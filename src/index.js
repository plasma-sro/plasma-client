import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./components/layouts/App";
import * as serviceWorker from "./loaders/serviceWorker";

ReactDOM.render(<App />, document.getElementById("root"));

serviceWorker.unregister();
