import React from "react";
import "./App.css";

//import { Provider } from "react-redux";
//import configureStore from "../../configs/configureStore";
import Router from "../../routes/router";

//const store = configureStore();

const App = () => {
  return (
    //<Provider store={store}>
    <>
      <h1>Hello world!</h1>
      <Router></Router>
    </>
    //</Provider>
  );
};

export default App;
